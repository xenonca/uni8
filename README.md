# uni8
Simple and plain texture pack with 8px textures.  
Feel free to [request](https://xenonca.github.io/contact.html) support for a mod or game.  
[![ContentDB](https://content.minetest.net/packages/_Xenon/uni8/shields/title/)](https://content.minetest.net/packages/_Xenon/uni8/) [![ContentDB](https://content.minetest.net/packages/_Xenon/uni8/shields/downloads/)](https://content.minetest.net/packages/_Xenon/uni8/)
***
## Support
The texture pack currently supports the following mods and games:  
- [MTG (Minetest Game)](https://content.minetest.net/packages/Minetest/minetest_game/)  
- [CTF](https://content.minetest.net/packages/rubenwardy/capturetheflag/)  
- [ethereal](https://content.minetest.net/packages/TenPlus1/ethereal/)  
- [LuckyDude](https://content.minetest.net/packages/xenonca/luckydude/)  
- [Handholds](https://content.minetest.net/packages/Shara/handholds/)  
- [HBHunger](https://content.minetest.net/packages/Wuzzy/hbhunger/)  
- [Hudbars](https://content.minetest.net/packages/Wuzzy/hudbars/)  
- [shooter](https://content.minetest.net/packages/stu/shooter/)  
- [grenades](https://content.minetest.net/packages/Lone_Wolf/grenades_basic/)  
- [crafting](https://content.minetest.net/packages/rubenwardy/crafting/)  

Planned support/features:  
- [darks](https://content.minetest.net/packages/_Xenon/darks/)  
- [lavastuff](https://content.minetest.net/packages/Lone_Wolf/lavastuff/)  
- Texture color overhaul

## Notes
Some textures are 16px textures which look like 8px textures. This enables support for other mods/games which use these textures in 16px on a model.  
Textures affected:
- wool_white.png (for CTF)
- default_wood.png (for CTF)

## License
Media: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by xenonca  

## Version
- v1.6 supprot for `CTFv3`
- v1.5.1 support for `luckydude`
- v1.5 support for `CTF`
- v1.4 support for `crafting`
- v1.3 support for `grenades`
- v1.2 support for `shooter`
- v1.1 support for `ethereal`
- v1.0 First Release

## Changelog
### v1.6
- support for `CTFv3`
- support `MTG` 5.5/5.6
- changed `default_chest` textures (for CTF compat)
- changed `gui_hotbar_selected` texture (for CTF compat)
- changed `torch` textures
- created `object_crosshair`
- created textures for `steel_bar_door` and `steel_bar_trap_door`
- created sun and moon textures
- updated GUI and HUD textures
- updated `gui_hb_bg`, `progress_bar`, `progress_bar_bg`, `creative_next_icon`, `creative_prev_icon`, wooden doors, mese postlight and wooden sign textures

### v1.5.1
- support for `LuckyDude`
- updated door textures
- updated sign textures
- created `ethereal_sakura_door` texture

### v1.5
- support for `CTFv2`

### v1.4
- support for `crafting`